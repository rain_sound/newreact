const globalConfig = require("../config");
/*
 PARAMS
 result object, message, error indication, 
 */
const sendJson = (res, message = {}, error = true, data = {}) => {
  return res
    .status(200)
    .json({ message: message, error: error, data: data });
};

module.exports = {
  sendJson
};
