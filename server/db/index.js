const Sequelize = require('sequelize')
const MovieModel = require('./models/movies.model')
const SeedDatabase = require('./factory/movies')

// File with database config 
const cf = require('../config')

// Initial set up sequelize - ORM
const conn = new Sequelize(cf.db_name, cf.user_db, cf.password_db, {
    host: cf.host,
    dialect: cf.dialect,
    pool: {
        max: cf.max_conn,
        min: cf.min_conn
    }
})

// Define Movie schema 
const Movie = MovieModel(conn, Sequelize)

// Initialize connection with database
conn.sync({ force: true })
    .then(() => {
        SeedDatabase(Movie, 10);
        console.log(`Tablas y database creados`)
    })
    .catch(err => {
        console.log(err);
    })

module.exports = {
    Movie
}
