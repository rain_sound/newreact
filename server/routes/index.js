
var router = require('express').Router()

// Set up router with movies routes { children routes }
const movies = require('./movies')

router.use('/movies', movies)

module.exports = router