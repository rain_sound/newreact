const { sendJson } = require("../../utilities");
const { Movie } = require("../../db");

const objTitle = "pelicula";

const errorLoading = `Error cargando los datos de las ${objTitle}s`;
const errorCreation = `Error al registrar la ${objTitle}`;
const errorUpdate = `Error al actualizar la ${objTitle}`;
const errorDelete = `Error al eliminar la ${objTitle}`;

const successCreation = `La ${objTitle} se registro con exito`;
const successUpdate = `La ${objTitle} se actualizo con exito`;
const successDelete = `La ${objTitle} se elimino con exito`;

const getAll = (req, res, next) => {
  Movie.findAll()
    .then(movies => sendJson(res, "", false, movies))
    .catch(err => sendJson(res, { dev: err, user: errorLoading }));
};

const getById = (req, res, next) => {
  const { id } = req.params;
  Movie.findOne({
    where: {
      id
    }
  })
    .then(movie => sendJson(res, "", false, movie))
    .catch(err => sendJson(res, { dev: err, user: errorLoading }));
};

const createMovie = (req, res, next) => {
  const { name, director, year, description, gender, imgProfile } = req.body;
  Movie.create({
    name,
    director,
    year,
    gender,
    imgProfile,
    description
  })
    .then(movie => sendJson(res, successCreation, false, movie))
    .catch(err => sendJson(res, { dev: err, user: errorCreation }));
};

const updateMovie = (req, res, next) => {
  const { id } = req.params;
  const { name, director, year, description, gender, imgProfile } = req.body;
  Movie.findOne({
    where: {
      id
    }
  })
    .then(movie => {
      if (movie) {
        movie
          .update({
            name,
            director,
            year,
            gender,
            imgProfile: "https://i.picsum.photos/id/455/200/300.jpg",
            description
          })
          .then(data => sendJson(res, successUpdate, false, data))
          .catch(err => sendJson(res, { dev: err, user: errorUpdate }));
      }
    })
    .catch(err => sendJson(res, { dev: err, user: errorUpdate }));
};

const deleteMovie = (req, res, next) => {
    const { id } = req.params
    
    Movie.destroy({
        where: {
            id
        }
    })
    .then(movie => sendJson(res, successDelete, false, id))
    .catch(err => sendJson(res, {"dev": err, user: errorDelete }))
};

module.exports = {
  getAll,
  getById,
  createMovie,
  updateMovie,
  deleteMovie
};
