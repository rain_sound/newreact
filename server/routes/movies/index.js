const router = require("express").Router();

const movieService = require("./movie.service");

router.get("/", movieService.getAll);
router.get("/:id", movieService.getById);
router.post("/new", movieService.createMovie);
router.put("/:id", movieService.updateMovie);
router.delete("/:id", movieService.deleteMovie);

module.exports = router;
