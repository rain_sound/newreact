import React, { Component } from "react";
import api from "./helpers/networking";
import { myContext } from "./helpers/context";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import ListMovies from "./components/ListMovie";

import "./App.css";

class ManageMovie extends Component {
  state = {
    name: "",
    gender: "",
    description: "",
    year: "",
    director: ""
  };

  _handleClick = e => {
    e.preventDefault();
    const { name, gender, description, year, director } = this.state;
    const { handleClick } = this.props;

    handleClick(name, gender, description, year, director);
  };

  _handleChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <div className="container card p-2">
        <form onSubmit={this._handleClick}>
          <div className="form-row mt-3">
            <div className="col-6">
              <input
                className="form-control"
                type="text"
                placeholder="Ingrese el nombre"
                onChange={this._handleChange}
                name="name"
              />
            </div>
            <div className="col-6">
              <input
                className="form-control"
                type="text"
                placeholder="Ingrese el genero"
                onChange={this._handleChange}
                name="gender"
              />
            </div>
          </div>
          <input
            className="form-control mt-3"
            type="text"
            placeholder="Ingrese una descripcion"
            onChange={this._handleChange}
            name="description"
          />
          <div className="form-row mt-3">
            <div className="col-6">
              <input
                className="form-control"
                type="date"
                placeholder="Ingrese el año"
                onChange={this._handleChange}
                name="year"
              />
            </div>
            <div className="col-6">
              <input
                className="form-control"
                type="text"
                placeholder="Ingrese el director"
                onChange={this._handleChange}
                name="director"
              />
            </div>
          </div>
          <div className="form-row mt-3 justify-content-center">
            <input className="btn btn-success" type="submit" name="" id="" />
          </div>
        </form>
      </div>
    );
  }
}

const Header = () => (
  <div className="title_head">
    <h1 className="title title--strong">Movies</h1>{" "}
    <span className="title title--sub">App</span>
  </div>
);

class App extends Component {
  state = {
    list: [],
    error: "",
    id: "",
    name: "",
    director: "",
    year: "",
    gender: "",
    description: "",
    modalUpdate: false
  };

  componentDidMount() {
    api
      .get()
      .then(res => {
        if (!res.error) {
          this.setState({ list: res.data.data });
        } else {
          this.setState({ error: res.error.user });
        }
      })
      .catch(err => this.setState({ error: err.error.user }));
  }

  handleDelete = id => {
    api
      .delete(`${id}`)
      .then(res => {
        if (!res.error) {
          this.setState(prevState => ({
            list: prevState.list.filter(item => item.id != res.data.data)
          }));
        } else {
          this.setState({ error: res.error.user });
        }
      })
      .catch(err => this.setState({ error: err.error.user }));
  };

  handleUpdateModal = e => {
    e.preventDefault();
    const { id, name, gender, description, director, year } = this.state;
    api
      .put(`${id}`, {
        name,
        director,
        year,
        gender,
        description
      })
      .then(res => {
        const { data } = res.data;
        if (!res.error) {
          this.setState(prevState => ({
            ...prevState,
            modalUpdate: false,
            list: prevState.list.map(item =>
              item.id == data.id
                ? {
                    ...item,
                    name: data.name,
                    director: data.director,
                    year: data.year,
                    gender: data.gender,
                    description: data.description
                  }
                : item
            )
          }));
        } else {
          this.setState({ error: res.error.user });
        }
      })
      .catch(err => this.setState({ error: err.error.user }));
  };

  handleUpdate = (id, name, director, year, gender, description) => {
    this.setState({
      id,
      name,
      director,
      year,
      gender,
      description,
      modalUpdate: true
    });
  };

  handleCreate = (name, gender, description, year, director) => {
    api
      .post("new", {
        name,
        gender,
        description,
        year,
        director
      })
      .then(res => {
        console.log("data", res);
        if (!res.data.error) {
          this.setState(prevState => ({
            ...prevState,
            list: [...prevState.list, res.data.data]
          }));
        }
      })
      .catch(err => this.setState({ error: err.error.user }));
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleToggle = () => {
    this.setState(prevState => ({ modalUpdate: !prevState.modalUpdate }));
  };

  render() {
    const {
      modalUpdate,
      list,
      id,
      name,
      gender,
      description,
      director,
      year
    } = this.state;

    return (
      <>
        <myContext.Provider
          value={{
            handleUpdate: this.handleUpdate,
            handleDelete: this.handleDelete
          }}
        >
          {/* MODAL DELETE */}
          {modalUpdate && (
            <div>
              <Modal isOpen={true} toggle={false}>
                <ModalHeader>Actualizar </ModalHeader>
                <ModalBody>
                  <form onSubmit={this.handleUpdateModal}>
                    <div className="form-row mt-3">
                      <div className="col-6">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Ingrese el nombre"
                          onChange={this.handleChange}
                          value={name}
                          name="name"
                        />
                      </div>
                      <div className="col-6">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Ingrese el genero"
                          onChange={this.handleChange}
                          value={gender}
                          name="gender"
                        />
                      </div>
                    </div>
                    <input
                      className="form-control mt-3"
                      type="text"
                      placeholder="Ingrese una descripcion"
                      onChange={this.handleChange}
                      value={description}
                      name="description"
                    />
                    <div className="form-row mt-3">
                      <div className="col-6">
                        <input
                          className="form-control"
                          type="date"
                          placeholder="Ingrese el año"
                          onChange={this.handleChange}
                          value={year}
                          name="year"
                        />
                      </div>
                      <div className="col-6">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="Ingrese el director"
                          onChange={this.handleChange}
                          value={director}
                          name="director"
                        />
                      </div>
                    </div>
                    <div className="form-row mt-3 justify-content-around">
                      <input
                        className="btn btn-success"
                        type="submit"
                        name=""
                        id=""
                      />
                      <button
                        onClick={this.handleToggle}
                        class="btn btn-danger"
                      >
                        Cerrar
                      </button>
                    </div>
                  </form>
                </ModalBody>
              </Modal>
            </div>
          )}

          <Header />
          <div className="container justify-content-center  mt-4">
            <ManageMovie handleClick={this.handleCreate} />
            {list.length !== 0 && <ListMovies list={list} />}
          </div>
        </myContext.Provider>
      </>
    );
  }
}

export default App;
