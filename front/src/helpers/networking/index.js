import axios from 'axios';

export const URL = 'http://localhost:3030/movies'

const client = axios.create({
    baseURL: URL,
    responseType: 'json',
})


export default client;
