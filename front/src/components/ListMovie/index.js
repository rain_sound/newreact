import React from "react";


import { myContext } from "../../helpers/context";

const ItemMovie = ({
  item: { id, name, director, year, gender, description, imgProfile }
}) => {
  return (
    <myContext.Consumer>
        
      {context => (
        <div className="card_movie card col-12 col-lg-8">
          <img className="card-img-top img-fluid" src={imgProfile} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              <b>Descripión:</b> {description}
            </p>
            <div className="row">
              <div className="col-12">
                <b>Director:</b> {director}
              </div>
              <div className="col-6">
                <b>Año:</b>
                {year}
              </div>
              <div className="col-6">
                <b>Género:</b> {gender}
              </div>
            </div>
          </div>
          <div className="card-body d-flex justify-content-around">
            <button
              onClick={e =>
                context.handleUpdate(
                  id,
                  name,
                  director,
                  year,
                  gender,
                  description
                )
              }
              className="btn btn-success"
            >
              Actualizar
            </button>
            <button
              onClick={e => context.handleDelete(id)}
              className="btn btn-danger"
            >
              Eliminar
            </button>
          </div>
         
        </div>
      )}
    </myContext.Consumer>
  );
};

const ListMovie = ({ list }) => {
  return (
    <div className="row justify-content-center">
      {list.map(item => (
        <ItemMovie key={item.id} item={item} />
      ))}
    </div>
  );
};

export default ListMovie;
